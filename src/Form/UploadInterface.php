<?php

namespace Drupal\image_upload\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image_upload\CompressImages;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Contribute form.
 */
class UploadInterface extends FormBase {

  protected $compress;

  /**
   * Implements __construct().
   *
   * @param Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The CompressImages service,adding to container.
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('image_upload.compress_image'));
  }

  /**
   * Implements __construct().
   *
   * @param Drupal\image_upload\CompressImages $compress
   *   The CompressImages service, injected.
   */
  public function __construct(CompressImages $compress) {
    $this->compress = $compress;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'image_upload_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['folder_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter Your Directory Name'),
      '#description' => $this->t('Enter your directory path inside files folder,please enter only after files folder e.g images with no slash at beginning,<br>If left blank images will be uploaded in files folde,if folder not exist it will create one inside files folder as entered.'),
    ];
    $form['jpeg_quality'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter JPEG Quality Parameter To compress'),
      '#description' => $this->t('Enter JPEG quality factor ranges from 0-100 any value e.g 10, It will compress your image size e.g 790kb images will compress to 190kb.'),
    ];
    $form['uploaded_file'] = [
      '#type' => 'file',
      '#title' => $this->t('Upload your file'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $source_image_folder = $form_state->getValue('folder_name');
    if (!file_exists('public://' . $source_image_folder)) {
      mkdir('public://' . $source_image_folder, 0777, TRUE);
    }

    $jpeg_qulity_factor = $form_state->getValue('jpeg_quality');
    if (empty($jpeg_qulity_factor)) {
      $jpeg_qulity_factor = 75;
    }
    if ($file = file_save_upload('uploaded_file', ['file_validate_extensions' => 'zip'], 'public://' . $source_image_folder, 0)) {
      $exturi = 'public://' . $source_image_folder;
      $uri = $file->getFileUri();
      $realpath = drupal_realpath($uri);
      chmod($realpath, 0777);
      $zip = new \ZipArchive();
      $zip->open($realpath);
      $product_file_path = '/sites/default/files/' . $source_image_folder . '/';
      @$zip->extractTo(drupal_realpath($exturi));
      for ($i = 0; $i < $zip->numFiles; $i++) {
        $stat = $zip->statIndex($i);
        $product_filename = getcwd() . $product_file_path . '' . $stat['name'];
        chmod($product_filename, 0777);
        $handle = fopen($product_filename, "rb");
        fclose($handle);
        $this->compress->compressImage($product_filename, $product_filename, $jpeg_qulity_factor);
      }
      drupal_unlink($realpath);
      drupal_set_message($this->t("File has been successfully uploaded!!"));
    }
  }

}
