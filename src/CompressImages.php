<?php

namespace Drupal\image_upload;

/**
 * Contains sercvice for compressing, jpg image size.
 */
class CompressImages {

  /**
   * Compression logic.
   */
  public function compressImage($source_url, $destination_url, $quality = 75) {
    $info = getimagesize($source_url);
    if ($info['mime'] == 'image/jpeg') {
      $image = imagecreatefromjpeg($source_url);
      imagejpeg($image, $destination_url, $quality);
    }
    else {
      drupal_unlink($source_url);
      $exploded_img = explode('/', $source_url);
      drupal_set_message($exploded_img[count($exploded_img) - 1] . $this->t('Image is not jpg hence not uploaded.'), 'status');
    }
    return $destination_url;
  }

}
