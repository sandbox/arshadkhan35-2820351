<?php

namespace Drupal\image_upload\Controller;

/**
 * Implements route default method for controller.
 */
class ImageUploadCotroller {

  /**
   * {@inheritdoc}
   */
  public function imgUpload() {
    $this->preSave();
    return \Drupal::formBuilder()->getForm('Drupal\image_upload\Form\UploadInterface');
  }

}
