INTRODUCTION
------------
The Bulk Image Uploader And Compressor module contains a user interface
i.e a form  for image upload and compress all images with .jpg extension.   
    * For a full description of the module, visit the project page:
    https://www.drupal.org/node/2820355

REQUIREMENTS
------------
    * PHP Zip library

RECOMMENDED MODULES
-------------------
    No Module Recommendation.

INSTALLATION
------------

    * Install as you would normally install a contributed Drupal module. 
    Visit:
        https://drupal.org/documentation/install/modules-themes/modules-7
        for further information.
    * Go to the Modules page (/admin/modules) and enable it.
    * Then  Go to  (/admin/structure/Upload Images) and click it. 

CONFIGURATION
-------------

    The module has no menu or modifiable settings. There is no configuration. 
    When enabled, the module will prevent the links from appearing. 
    To get the links back, disable the module and clear caches.

TROUBLESHOOTING
---------------

    * If the menu does not display, check the following:

       - Are the "Access administration menu" and "Use the administration pages
         and help" permissions enabled for the appropriate roles?

FAQ
---
    Q: I Uploaded bulk of images ,all the images have been uploaded successfully 
    but only .jpg extensions images are being compressed?
    A: Yes, this is the intended behavior. only .jpg images will be compressed.

MAINTAINERS
-----------
    Current maintainers:
    * Mohammad Arshad khan (arshadkhan35) - https://www.drupal.org/user/3341832
